local wezterm = require 'wezterm'

return {
	font = wezterm.font 'Hasklug Nerd Font',
	color_scheme = "Dracula",
	hide_tab_bar_if_only_one_tab = true,
	window_padding = { top = 0, right = 0, bottom = 0, left = 0 }
}
